import React, { useState } from "react";
import axiosInstance from '../axiosConfig';

function AddSportForm({ onSportAdded }) {
  const [name, setName] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name.trim() === "") {
      setError("Sport name cannot be blank");
      return;
    }
    try {
      await axiosInstance.post(
        "/sports",
        { name },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setName("");
      setError("");
      onSportAdded();
    } catch (error) {
      console.error("Error adding sport:", error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Sport Name"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      {error && <p style={{ color: "red" }}>{error}</p>}
      <button type="submit">Add Sport</button>
    </form>
  );
}

export default AddSportForm;
