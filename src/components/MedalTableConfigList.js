import React, { useState, useEffect } from "react";
import axiosInstance from '../axiosConfig';
import AddMedalTableConfigForm from "./AddMedalTableConfigForm";
import EditMedalTableConfigDialog from "./EditMedalTableConfigDialog";
import DeleteConfirmationDialog from "./DeleteConfirmationDialog";

function MedalTableConfigList() {
  const [configs, setConfigs] = useState([]);
  const [sports, setSports] = useState([]);
  const [selectedConfig, setSelectedConfig] = useState(null);
  const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const [configToDelete, setConfigToDelete] = useState(null);
  const [filter, setFilter] = useState({
    name: "",
    sportId: "",
    tier: "",
  });
  const [timeoutId, setTimeoutId] = useState(null);

  useEffect(() => {
    fetchConfigs();
    fetchSports();
  }, []);

  useEffect(() => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    const newTimeoutId = setTimeout(() => {
      fetchConfigs();
    }, 1000);
    setTimeoutId(newTimeoutId);
  }, [filter]);

  const fetchConfigs = async () => {
    try {
      const response = await axiosInstance.get(
        "/medal-table-configs",
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          params: filter,
        }
      );
      setConfigs(response.data);
    } catch (error) {
      console.error("Error fetching medal table configs:", error);
    }
  };

  const fetchSports = async () => {
    try {
      const response = await axiosInstance.get("/sports", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setSports(response.data);
    } catch (error) {
      console.error("Error fetching sports:", error);
    }
  };

  const handleEditConfig = (config) => {
    setSelectedConfig(config);
  };

  const handleCloseDialog = () => {
    setSelectedConfig(null);
  };

  const handleDeleteConfirmation = (config) => {
    setConfigToDelete(config);
    setDeleteConfirmationOpen(true);
  };

  const handleDeleteCancel = () => {
    setConfigToDelete(null);
    setDeleteConfirmationOpen(false);
  };

  const handleDeleteConfirm = async () => {
    try {
      await axiosInstance.delete(
        `/medal-table-configs/${configToDelete.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      fetchConfigs();
      setDeleteConfirmationOpen(false);
    } catch (error) {
      console.error("Error deleting medal table config:", error);
    }
  };

  const handleRecalculateClick = async (configId) => {
    try {
      await axiosInstance.post(
        `/medal-table-configs/${configId}/recalculate`,
        null,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      // Handle success, e.g., show a success message or refresh the medal table
    } catch (error) {
      console.error("Error recalculating medal table:", error);
      // Handle error, e.g., show an error message
    }
  };

  return (
    <div>
      <h2>Medal Table Configuration List</h2>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div>
          <input
            type="text"
            placeholder="Filter by name"
            value={filter.name}
            onChange={(e) => setFilter({ ...filter, name: e.target.value })}
          />
          <select
            value={filter.sportId}
            onChange={(e) => setFilter({ ...filter, sportId: e.target.value })}
          >
            <option value="">All Sports</option>
            {sports.map((sport) => (
              <option key={sport.id} value={sport.id}>
                {sport.name}
              </option>
            ))}
          </select>
          <select
            value={filter.tier}
            onChange={(e) => setFilter({ ...filter, tier: e.target.value })}
          >
            <option value="">All Tiers</option>
            <option value="1">Tier 1</option>
            <option value="2">Tier 2</option>
            <option value="3">Tier 3</option>
          </select>
        </div>
        <AddMedalTableConfigForm onConfigAdded={fetchConfigs} sports={sports} />
      </div>
      <ul>
        {configs.map((config) => (
          <li key={config.id}>
            {config.name} - Sport: {config.sport_name || "N/A"}, Tier:{" "}
            {config.tier || "N/A"}
            <button onClick={() => handleEditConfig(config)}>Edit</button>
            <button onClick={() => handleDeleteConfirmation(config)}>
              Delete
            </button>
            <button onClick={() => handleRecalculateClick(config.id)}>
              Re-Calculate
            </button>
          </li>
        ))}
      </ul>
      {selectedConfig && (
        <EditMedalTableConfigDialog
          config={selectedConfig}
          onClose={handleCloseDialog}
          onConfigUpdated={fetchConfigs}
          sports={sports}
        />
      )}
      {deleteConfirmationOpen && (
        <DeleteConfirmationDialog
          onCancel={handleDeleteCancel}
          onConfirm={handleDeleteConfirm}
        />
      )}
    </div>
  );
}

export default MedalTableConfigList;
