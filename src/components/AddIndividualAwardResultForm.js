import React, { useState } from "react";
import axiosInstance from "../axiosConfig";
import Select from "react-select";

function AddIndividualAwardResultForm({
  onIndividualAwardResultAdded,
  individualAwards,
  managers,
}) {
  const [individualAwardId, setIndividualAwardId] = useState("");
  const [managerId, setManagerId] = useState("");
  const [startYear, setStartYear] = useState("");
  const [endYear, setEndYear] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      individualAwardId === "" ||
      managerId === "" ||
      startYear === "" ||
      endYear === ""
    ) {
      setError("Please fill in all fields");
      return;
    }
    if (isNaN(startYear) || startYear.length !== 4) {
      setError("Start Year must be a valid year (YYYY)");
      return;
    }
    if (isNaN(endYear) || endYear.length !== 4) {
      setError("End Year must be a valid year (YYYY)");
      return;
    }
    if (parseInt(endYear) < parseInt(startYear)) {
      setError("End Year must be greater than or equal to Start Year");
      return;
    }
    try {
      await axiosInstance.post(
        "/individual-award-results",
        {
          individualAwardId,
          managerId,
          startYear,
          endYear,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setIndividualAwardId("");
      setManagerId("");
      setStartYear("");
      setEndYear("");
      setError("");
      onIndividualAwardResultAdded();
    } catch (error) {
      console.error("Error adding individual award result:", error);
    }
  };

  const individualAwardOptions = individualAwards.map((individualAward) => ({
    value: individualAward.id,
    label: individualAward.name,
  }));

  const managerOptions = managers.map((manager) => ({
    value: manager.id,
    label: manager.name,
  }));

  return (
    <form onSubmit={handleSubmit}>
      <Select
        options={individualAwardOptions}
        value={individualAwardOptions.find(
          (option) => option.value === individualAwardId
        )}
        onChange={(selectedOption) =>
          setIndividualAwardId(selectedOption.value)
        }
        placeholder="Select Individual Award"
        required
      />
      <Select
        options={managerOptions}
        value={managerOptions.find((option) => option.value === managerId)}
        onChange={(selectedOption) => setManagerId(selectedOption.value)}
        placeholder="Select Manager"
        required
      />
      <input
        type="text"
        placeholder="Start Year (YYYY)"
        value={startYear}
        onChange={(e) => setStartYear(e.target.value)}
      />
      <input
        type="text"
        placeholder="End Year (YYYY)"
        value={endYear}
        onChange={(e) => setEndYear(e.target.value)}
      />
      {error && <p style={{ color: "red" }}>{error}</p>}
      <button type="submit">Add Individual Award Result</button>
    </form>
  );
}

export default AddIndividualAwardResultForm;
