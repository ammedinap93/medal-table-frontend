import React, { useState, useEffect } from "react";
import axiosInstance from '../axiosConfig';
import Select from "react-select";
import "../styles/Dialog.css";

function EditResultDialog({
  result,
  onClose,
  onResultUpdated,
  leagues,
  managers,
}) {
  const [leagueId, setLeagueId] = useState(result.league_id);
  const [goldManagers, setGoldManagers] = useState([]);
  const [silverManagers, setSilverManagers] = useState([]);
  const [bronzeManagers, setBronzeManagers] = useState([]);
  const [error, setError] = useState("");

  useEffect(() => {
    const fetchManagerNames = async () => {
      try {
        const goldManagerNames = await Promise.all(
          result.gold_managers.map(async (managerId) => {
            const response = await axiosInstance.get(
              `/managers/${managerId}`,
              {
                headers: {
                  Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
              }
            );
            return { value: managerId, label: response.data.name };
          })
        );
        setGoldManagers(goldManagerNames);

        const silverManagerNames = await Promise.all(
          result.silver_managers.map(async (managerId) => {
            const response = await axiosInstance.get(
              `/managers/${managerId}`,
              {
                headers: {
                  Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
              }
            );
            return { value: managerId, label: response.data.name };
          })
        );
        setSilverManagers(silverManagerNames);

        const bronzeManagerNames = await Promise.all(
          result.bronze_managers.map(async (managerId) => {
            const response = await axiosInstance.get(
              `/managers/${managerId}`,
              {
                headers: {
                  Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
              }
            );
            return { value: managerId, label: response.data.name };
          })
        );
        setBronzeManagers(bronzeManagerNames);
      } catch (error) {
        console.error("Error fetching manager names:", error);
      }
    };

    fetchManagerNames();
  }, [result]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      leagueId === "" ||
      (goldManagers.length === 0 &&
        silverManagers.length === 0 &&
        bronzeManagers.length === 0)
    ) {
      setError("Please fill in all required fields");
      return;
    }
    try {
      await axiosInstance.put(
        `/results/${result.id}`,
        {
          leagueId,
          goldManagers: goldManagers.map((manager) => manager.value),
          silverManagers: silverManagers.map((manager) => manager.value),
          bronzeManagers: bronzeManagers.map((manager) => manager.value),
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      onResultUpdated();
      onClose();
    } catch (error) {
      console.error("Error updating result:", error);
    }
  };

  const leagueOptions = leagues.map((league) => ({
    value: league.id,
    label: `${league.name} (${
      league.start_year === league.end_year
        ? league.start_year
        : `${league.start_year}-${league.end_year}`
    })`,
  }));

  const managerOptions = managers.map((manager) => ({
    value: manager.id,
    label: manager.name,
  }));

  return (
    <div className="dialog-overlay" onClick={onClose}>
      <div className="dialog-content" onClick={(e) => e.stopPropagation()}>
        <h3>Edit Result</h3>
        <form onSubmit={handleSubmit}>
          <Select
            options={leagueOptions}
            value={leagueOptions.find((option) => option.value === leagueId)}
            onChange={(selectedOption) => setLeagueId(selectedOption.value)}
            placeholder="Select League"
            required
          />
          <Select
            options={managerOptions}
            value={goldManagers}
            onChange={(selectedOptions) => setGoldManagers(selectedOptions)}
            isMulti
            placeholder="Select Gold Managers"
          />
          <Select
            options={managerOptions}
            value={silverManagers}
            onChange={(selectedOptions) => setSilverManagers(selectedOptions)}
            isMulti
            placeholder="Select Silver Managers"
          />
          <Select
            options={managerOptions}
            value={bronzeManagers}
            onChange={(selectedOptions) => setBronzeManagers(selectedOptions)}
            isMulti
            placeholder="Select Bronze Managers"
          />
          {error && <p style={{ color: "red" }}>{error}</p>}
          <div className="dialog-actions">
            <button type="submit">Update</button>
            <button type="button" onClick={onClose}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditResultDialog;
