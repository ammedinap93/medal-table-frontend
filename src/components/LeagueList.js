import React, { useState, useEffect } from "react";
import axiosInstance from '../axiosConfig';
import AddLeagueForm from "./AddLeagueForm";
import EditLeagueDialog from "./EditLeagueDialog";
import DeleteConfirmationDialog from "./DeleteConfirmationDialog";

function LeagueList() {
  const [leagues, setLeagues] = useState([]);
  const [selectedLeague, setSelectedLeague] = useState(null);
  const [filter, setFilter] = useState({
    name: "",
    competitionId: "",
    sportId: "",
    tier: "",
    startYear: "",
    endYear: "",
  });
  const [competitions, setCompetitions] = useState([]);
  const [sports, setSports] = useState([]);
  const [timeoutId, setTimeoutId] = useState(null);
  const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const [leagueToDelete, setLeagueToDelete] = useState(null);

  useEffect(() => {
    fetchLeagues();
    fetchCompetitions();
    fetchSports();
  }, []);

  useEffect(() => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    const newTimeoutId = setTimeout(() => {
      fetchLeagues();
    }, 1000);
    setTimeoutId(newTimeoutId);
  }, [filter]);

  const fetchLeagues = async () => {
    try {
      const response = await axiosInstance.get("/leagues", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        params: filter,
      });
      setLeagues(response.data);
    } catch (error) {
      console.error("Error fetching leagues:", error);
    }
  };

  const fetchCompetitions = async () => {
    try {
      const response = await axiosInstance.get("/competitions", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setCompetitions(response.data);
    } catch (error) {
      console.error("Error fetching competitions:", error);
    }
  };

  const fetchSports = async () => {
    try {
      const response = await axiosInstance.get("/sports", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setSports(response.data);
    } catch (error) {
      console.error("Error fetching sports:", error);
    }
  };

  const handleEditLeague = (league) => {
    setSelectedLeague(league);
  };

  const handleCloseDialog = () => {
    setSelectedLeague(null);
  };

  const handleDeleteConfirmation = (league) => {
    setLeagueToDelete(league);
    setDeleteConfirmationOpen(true);
  };

  const handleDeleteCancel = () => {
    setLeagueToDelete(null);
    setDeleteConfirmationOpen(false);
  };

  const handleDeleteConfirm = async () => {
    try {
      await axiosInstance.delete(`/leagues/${leagueToDelete.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      fetchLeagues();
      setDeleteConfirmationOpen(false);
    } catch (error) {
      console.error("Error deleting league:", error);
    }
  };

  return (
    <div>
      <h2>League List</h2>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div>
          <input
            type="text"
            placeholder="Filter by name"
            value={filter.name}
            onChange={(e) => setFilter({ ...filter, name: e.target.value })}
          />
          <select
            value={filter.sportId}
            onChange={(e) =>
              setFilter({
                ...filter,
                sportId: e.target.value,
                competitionId: "",
              })
            }
          >
            <option value="">All Sports</option>
            {sports.map((sport) => (
              <option key={sport.id} value={sport.id}>
                {sport.name}
              </option>
            ))}
          </select>
          <select
            value={filter.competitionId}
            onChange={(e) =>
              setFilter({ ...filter, competitionId: e.target.value })
            }
          >
            <option value="">All Competitions</option>
            {competitions
              .filter(
                (competition) =>
                  !filter.sportId ||
                  competition.sport_id === parseInt(filter.sportId)
              )
              .map((competition) => (
                <option key={competition.id} value={competition.id}>
                  {competition.name} ({competition.sport_name})
                </option>
              ))}
          </select>
          <select
            value={filter.tier}
            onChange={(e) => setFilter({ ...filter, tier: e.target.value })}
          >
            <option value="">All Tiers</option>
            <option value="1">Tier 1</option>
            <option value="2">Tier 2</option>
            <option value="3">Tier 3</option>
          </select>
          <input
            type="text"
            placeholder="Start Year (YYYY)"
            value={filter.startYear}
            onChange={(e) =>
              setFilter({ ...filter, startYear: e.target.value })
            }
          />
          <input
            type="text"
            placeholder="End Year (YYYY)"
            value={filter.endYear}
            onChange={(e) => setFilter({ ...filter, endYear: e.target.value })}
          />
        </div>
        <AddLeagueForm
          onLeagueAdded={fetchLeagues}
          competitions={competitions}
        />
      </div>
      <ul>
        {leagues.map((league) => (
          <li key={league.id}>
            {league.name} (
            {league.start_year === league.end_year
              ? league.start_year
              : `${league.start_year}-${league.end_year}`}
            ) - {league.competition_name} ({league.sport_name}) - Tier{" "}
            {league.tier}
            <button onClick={() => handleEditLeague(league)}>Edit</button>
            <button onClick={() => handleDeleteConfirmation(league)}>
              Delete
            </button>
          </li>
        ))}
      </ul>
      {selectedLeague && (
        <EditLeagueDialog
          league={selectedLeague}
          onClose={handleCloseDialog}
          onLeagueUpdated={fetchLeagues}
          competitions={competitions}
        />
      )}
      {deleteConfirmationOpen && (
        <DeleteConfirmationDialog
          onCancel={handleDeleteCancel}
          onConfirm={handleDeleteConfirm}
        />
      )}
    </div>
  );
}

export default LeagueList;
