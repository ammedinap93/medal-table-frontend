import React, { useState } from "react";
import axiosInstance from "../axiosConfig";

function AddIndividualAwardForm({ onIndividualAwardAdded }) {
  const [name, setName] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name.trim() === "") {
      setError("Please fill in all fields");
      return;
    }    
    try {
      await axiosInstance.post(
        "/individual-awards",
        {
          name,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setName("");
      setError("");
      onIndividualAwardAdded();
    } catch (error) {
      console.error("Error adding individual award:", error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Individual Award Name"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />      
      {error && <p style={{ color: "red" }}>{error}</p>}
      <button type="submit">Add Individual Award</button>
    </form>
  );
}

export default AddIndividualAwardForm;
