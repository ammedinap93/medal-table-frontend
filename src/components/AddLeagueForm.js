import React, { useState } from "react";
import axiosInstance from '../axiosConfig';

function AddLeagueForm({ onLeagueAdded, competitions }) {
  const [name, setName] = useState("");
  const [competitionId, setCompetitionId] = useState("");
  const [tier, setTier] = useState("");
  const [startYear, setStartYear] = useState("");
  const [endYear, setEndYear] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      name.trim() === "" ||
      competitionId === "" ||
      tier === "" ||
      startYear === "" ||
      endYear === ""
    ) {
      setError("Please fill in all fields");
      return;
    }
    if (isNaN(startYear) || startYear.length !== 4) {
      setError("Start Year must be a valid year (YYYY)");
      return;
    }
    if (isNaN(endYear) || endYear.length !== 4) {
      setError("End Year must be a valid year (YYYY)");
      return;
    }
    if (parseInt(endYear) < parseInt(startYear)) {
      setError("End Year must be greater than or equal to Start Year");
      return;
    }
    try {
      await axiosInstance.post(
        "/leagues",
        {
          name,
          competitionId,
          tier,
          startYear,
          endYear,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setName("");
      setCompetitionId("");
      setTier("");
      setStartYear("");
      setEndYear("");
      setError("");
      onLeagueAdded();
    } catch (error) {
      console.error("Error adding league:", error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="League Name"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <select
        value={competitionId}
        onChange={(e) => setCompetitionId(e.target.value)}
      >
        <option value="">Select Competition</option>
        {competitions.map((competition) => (
          <option key={competition.id} value={competition.id}>
            {competition.name} ({competition.sport_name})
          </option>
        ))}
      </select>
      <select value={tier} onChange={(e) => setTier(e.target.value)}>
        <option value="">Select Tier</option>
        <option value="1">Tier 1</option>
        <option value="2">Tier 2</option>
        <option value="3">Tier 3</option>
      </select>
      <input
        type="text"
        placeholder="Start Year (YYYY)"
        value={startYear}
        onChange={(e) => setStartYear(e.target.value)}
      />
      <input
        type="text"
        placeholder="End Year (YYYY)"
        value={endYear}
        onChange={(e) => setEndYear(e.target.value)}
      />
      {error && <p style={{ color: "red" }}>{error}</p>}
      <button type="submit">Add League</button>
    </form>
  );
}

export default AddLeagueForm;
