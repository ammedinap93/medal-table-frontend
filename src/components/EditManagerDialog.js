import React, { useState } from "react";
import axiosInstance from '../axiosConfig';
import "../styles/Dialog.css";

function EditManagerDialog({ manager, onClose, onManagerUpdated }) {
  const [name, setName] = useState(manager.name);
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name.trim() === "") {
      setError("Manager name cannot be blank");
      return;
    }
    try {
      await axiosInstance.put(
        `/managers/${manager.id}`,
        { name },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      onManagerUpdated();
      onClose();
    } catch (error) {
      console.error("Error updating manager:", error);
    }
  };

  return (
    <div className="dialog-overlay" onClick={onClose}>
      <div className="dialog-content" onClick={(e) => e.stopPropagation()}>
        <h3>Edit Manager</h3>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Manager Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          {error && <p style={{ color: "red" }}>{error}</p>}
          <div className="dialog-actions">
            <button type="submit">Rename</button>
            <button type="button" onClick={onClose}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditManagerDialog;
