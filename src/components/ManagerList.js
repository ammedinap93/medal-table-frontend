import React, { useState, useEffect } from "react";
import axiosInstance from '../axiosConfig';
import AddManagerForm from "./AddManagerForm";
import EditManagerDialog from "./EditManagerDialog";
import DeleteConfirmationDialog from "./DeleteConfirmationDialog";

function ManagerList() {
  const [managers, setManagers] = useState([]);
  const [filter, setFilter] = useState("");
  const [selectedManager, setSelectedManager] = useState(null);
  const [timeoutId, setTimeoutId] = useState(null);
  const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const [managerToDelete, setManagerToDelete] = useState(null);

  useEffect(() => {
    fetchManagers();
  }, []);

  useEffect(() => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    const newTimeoutId = setTimeout(() => {
      fetchManagers();
    }, 1000);
    setTimeoutId(newTimeoutId);
  }, [filter]);

  const fetchManagers = async () => {
    try {
      const response = await axiosInstance.get("/managers", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        params: { filter },
      });
      setManagers(response.data);
    } catch (error) {
      console.error("Error fetching managers:", error);
    }
  };

  const handleEditManager = (manager) => {
    setSelectedManager(manager);
  };

  const handleCloseDialog = () => {
    setSelectedManager(null);
  };

  const handleDeleteConfirmation = (manager) => {
    setManagerToDelete(manager);
    setDeleteConfirmationOpen(true);
  };

  const handleDeleteCancel = () => {
    setManagerToDelete(null);
    setDeleteConfirmationOpen(false);
  };

  const handleDeleteConfirm = async () => {
    try {
      await axiosInstance.delete(
        `/managers/${managerToDelete.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      fetchManagers();
      setDeleteConfirmationOpen(false);
    } catch (error) {
      console.error("Error deleting manager:", error);
    }
  };

  return (
    <div>
      <h2>Manager List</h2>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <input
          type="text"
          placeholder="Filter managers"
          value={filter}
          onChange={(e) => setFilter(e.target.value)}
        />
        <AddManagerForm onManagerAdded={fetchManagers} />
      </div>
      <ul>
        {managers.map((manager) => (
          <li key={manager.id}>
            {manager.name}
            <button onClick={() => handleEditManager(manager)}>Edit</button>
            <button onClick={() => handleDeleteConfirmation(manager)}>
              Delete
            </button>
          </li>
        ))}
      </ul>
      {selectedManager && (
        <EditManagerDialog
          manager={selectedManager}
          onClose={handleCloseDialog}
          onManagerUpdated={fetchManagers}
        />
      )}
      {deleteConfirmationOpen && (
        <DeleteConfirmationDialog
          onCancel={handleDeleteCancel}
          onConfirm={handleDeleteConfirm}
        />
      )}
    </div>
  );
}

export default ManagerList;
