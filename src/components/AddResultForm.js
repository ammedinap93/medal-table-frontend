import React, { useState } from "react";
import axiosInstance from '../axiosConfig';
import Select from "react-select";

function AddResultForm({ onResultAdded, leagues, managers }) {
  const [leagueId, setLeagueId] = useState("");
  const [goldManagers, setGoldManagers] = useState([]);
  const [silverManagers, setSilverManagers] = useState([]);
  const [bronzeManagers, setBronzeManagers] = useState([]);
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      leagueId === "" ||
      (goldManagers.length === 0 &&
        silverManagers.length === 0 &&
        bronzeManagers.length === 0)
    ) {
      setError("Please fill in all required fields");
      return;
    }
    try {
      await axiosInstance.post(
        "/results",
        {
          leagueId,
          goldManagers: goldManagers.map((manager) => manager.value),
          silverManagers: silverManagers.map((manager) => manager.value),
          bronzeManagers: bronzeManagers.map((manager) => manager.value),
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setLeagueId("");
      setGoldManagers([]);
      setSilverManagers([]);
      setBronzeManagers([]);
      setError("");
      onResultAdded();
    } catch (error) {
      console.error("Error adding result:", error);
    }
  };

  const leagueOptions = leagues.map((league) => ({
    value: league.id,
    label: `${league.name} (${
      league.start_year === league.end_year
        ? league.start_year
        : `${league.start_year}-${league.end_year}`
    })`,
  }));

  const managerOptions = managers.map((manager) => ({
    value: manager.id,
    label: manager.name,
  }));

  return (
    <form onSubmit={handleSubmit}>
      <Select
        options={leagueOptions}
        value={leagueOptions.find((option) => option.value === leagueId)}
        onChange={(selectedOption) => setLeagueId(selectedOption.value)}
        placeholder="Select League"
        required
      />
      <Select
        options={managerOptions}
        value={goldManagers}
        onChange={(selectedOptions) => setGoldManagers(selectedOptions)}
        isMulti
        placeholder="Select Gold Managers"
      />
      <Select
        options={managerOptions}
        value={silverManagers}
        onChange={(selectedOptions) => setSilverManagers(selectedOptions)}
        isMulti
        placeholder="Select Silver Managers"
      />
      <Select
        options={managerOptions}
        value={bronzeManagers}
        onChange={(selectedOptions) => setBronzeManagers(selectedOptions)}
        isMulti
        placeholder="Select Bronze Managers"
      />
      {error && <p style={{ color: "red" }}>{error}</p>}
      <button type="submit">Add Result</button>
    </form>
  );
}

export default AddResultForm;
