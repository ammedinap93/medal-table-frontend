import React, { useState, useEffect } from "react";
import axiosInstance from '../axiosConfig';
import AddCompetitionForm from "./AddCompetitionForm";
import EditCompetitionDialog from "./EditCompetitionDialog";
import DeleteConfirmationDialog from "./DeleteConfirmationDialog";

function CompetitionList() {
  const [competitions, setCompetitions] = useState([]);
  const [selectedCompetition, setSelectedCompetition] = useState(null);
  const [filter, setFilter] = useState({ name: "", sportId: "" });
  const [sports, setSports] = useState([]);
  const [timeoutId, setTimeoutId] = useState(null);
  const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const [competitionToDelete, setCompetitionToDelete] = useState(null);

  useEffect(() => {
    fetchCompetitions();
    fetchSports();
  }, []);

  useEffect(() => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    const newTimeoutId = setTimeout(() => {
      fetchCompetitions();
    }, 1000);
    setTimeoutId(newTimeoutId);
  }, [filter]);

  const fetchCompetitions = async () => {
    try {
      const response = await axiosInstance.get("/competitions", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        params: filter,
      });
      setCompetitions(response.data);
    } catch (error) {
      console.error("Error fetching competitions:", error);
    }
  };

  const fetchSports = async () => {
    try {
      const response = await axiosInstance.get("/sports", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setSports(response.data);
    } catch (error) {
      console.error("Error fetching sports:", error);
    }
  };

  const handleEditCompetition = (competition) => {
    setSelectedCompetition(competition);
  };

  const handleCloseDialog = () => {
    setSelectedCompetition(null);
  };

  const handleDeleteConfirmation = (competition) => {
    setCompetitionToDelete(competition);
    setDeleteConfirmationOpen(true);
  };

  const handleDeleteCancel = () => {
    setCompetitionToDelete(null);
    setDeleteConfirmationOpen(false);
  };

  const handleDeleteConfirm = async () => {
    try {
      await axiosInstance.delete(
        `/competitions/${competitionToDelete.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      fetchCompetitions();
      setDeleteConfirmationOpen(false);
    } catch (error) {
      console.error("Error deleting competition:", error);
    }
  };

  return (
    <div>
      <h2>Competition List</h2>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div>
          <input
            type="text"
            placeholder="Filter by name"
            value={filter.name}
            onChange={(e) => setFilter({ ...filter, name: e.target.value })}
          />
          <select
            value={filter.sportId}
            onChange={(e) => setFilter({ ...filter, sportId: e.target.value })}
          >
            <option value="">All Sports</option>
            {sports.map((sport) => (
              <option key={sport.id} value={sport.id}>
                {sport.name}
              </option>
            ))}
          </select>
        </div>
        <AddCompetitionForm
          onCompetitionAdded={fetchCompetitions}
          sports={sports}
        />
      </div>
      <ul>
        {competitions.map((competition) => (
          <li key={competition.id}>
            {competition.name} - {competition.sport_name}
            <button onClick={() => handleEditCompetition(competition)}>
              Edit
            </button>
            <button onClick={() => handleDeleteConfirmation(competition)}>
              Delete
            </button>
          </li>
        ))}
      </ul>
      {selectedCompetition && (
        <EditCompetitionDialog
          competition={selectedCompetition}
          onClose={handleCloseDialog}
          onCompetitionUpdated={fetchCompetitions}
          sports={sports}
        />
      )}
      {deleteConfirmationOpen && (
        <DeleteConfirmationDialog
          onCancel={handleDeleteCancel}
          onConfirm={handleDeleteConfirm}
        />
      )}
    </div>
  );
}

export default CompetitionList;
