import React, { useState, useEffect } from "react";
import axiosInstance from '../axiosConfig';
import AddResultForm from "./AddResultForm";
import EditResultDialog from "./EditResultDialog";
import DeleteConfirmationDialog from "./DeleteConfirmationDialog";

function ResultList() {
  const [results, setResults] = useState([]);
  const [leagues, setLeagues] = useState([]);
  const [sports, setSports] = useState([]);
  const [competitions, setCompetitions] = useState([]);
  const [managers, setManagers] = useState([]);
  const [selectedResult, setSelectedResult] = useState(null);
  const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const [resultToDelete, setResultToDelete] = useState(null);
  const [filter, setFilter] = useState({
    leagueName: "",
    sportId: "",
    competitionId: "",
    tier: "",
    startYear: "",
    endYear: "",
    managerId: "",
  });
  const [timeoutId, setTimeoutId] = useState(null);

  useEffect(() => {
    fetchResults();
    fetchLeagues();
    fetchSports();
    fetchCompetitions();
    fetchManagers();
  }, []);

  useEffect(() => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    const newTimeoutId = setTimeout(() => {
      fetchResults();
    }, 1000);
    setTimeoutId(newTimeoutId);
  }, [filter]);

  const fetchResults = async () => {
    try {
      const response = await axiosInstance.get("/results", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        params: filter,
      });
      setResults(response.data);
    } catch (error) {
      console.error("Error fetching results:", error);
    }
  };

  const fetchLeagues = async () => {
    try {
      const response = await axiosInstance.get("/leagues", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setLeagues(response.data);
    } catch (error) {
      console.error("Error fetching leagues:", error);
    }
  };

  const fetchSports = async () => {
    try {
      const response = await axiosInstance.get("/sports", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setSports(response.data);
    } catch (error) {
      console.error("Error fetching sports:", error);
    }
  };

  const fetchCompetitions = async () => {
    try {
      const response = await axiosInstance.get("/competitions", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setCompetitions(response.data);
    } catch (error) {
      console.error("Error fetching competitions:", error);
    }
  };

  const fetchManagers = async () => {
    try {
      const response = await axiosInstance.get("/managers", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setManagers(response.data);
    } catch (error) {
      console.error("Error fetching managers:", error);
    }
  };

  const handleEditResult = (result) => {
    setSelectedResult(result);
  };

  const handleCloseDialog = () => {
    setSelectedResult(null);
  };

  const handleDeleteConfirmation = (result) => {
    setResultToDelete(result);
    setDeleteConfirmationOpen(true);
  };

  const handleDeleteCancel = () => {
    setResultToDelete(null);
    setDeleteConfirmationOpen(false);
  };

  const handleDeleteConfirm = async () => {
    try {
      await axiosInstance.delete(`/results/${resultToDelete.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      fetchResults();
      setDeleteConfirmationOpen(false);
    } catch (error) {
      console.error("Error deleting result:", error);
    }
  };

  return (
    <div>
      <h2>Result List</h2>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div>
          <input
            type="text"
            placeholder="Filter by league name"
            value={filter.leagueName}
            onChange={(e) =>
              setFilter({ ...filter, leagueName: e.target.value })
            }
          />
          <select
            value={filter.sportId}
            onChange={(e) =>
              setFilter({
                ...filter,
                sportId: e.target.value,
                competitionId: "",
              })
            }
          >
            <option value="">All Sports</option>
            {sports.map((sport) => (
              <option key={sport.id} value={sport.id}>
                {sport.name}
              </option>
            ))}
          </select>
          <select
            value={filter.competitionId}
            onChange={(e) =>
              setFilter({ ...filter, competitionId: e.target.value })
            }
          >
            <option value="">All Competitions</option>
            {competitions
              .filter(
                (competition) =>
                  !filter.sportId ||
                  competition.sport_id === parseInt(filter.sportId)
              )
              .map((competition) => (
                <option key={competition.id} value={competition.id}>
                  {competition.name} ({competition.sport_name})
                </option>
              ))}
          </select>
          <select
            value={filter.tier}
            onChange={(e) => setFilter({ ...filter, tier: e.target.value })}
          >
            <option value="">All Tiers</option>
            <option value="1">Tier 1</option>
            <option value="2">Tier 2</option>
            <option value="3">Tier 3</option>
          </select>
          <input
            type="text"
            placeholder="Start Year (YYYY)"
            value={filter.startYear}
            onChange={(e) =>
              setFilter({ ...filter, startYear: e.target.value })
            }
          />
          <input
            type="text"
            placeholder="End Year (YYYY)"
            value={filter.endYear}
            onChange={(e) => setFilter({ ...filter, endYear: e.target.value })}
          />
          <select
            value={filter.managerId}
            onChange={(e) =>
              setFilter({ ...filter, managerId: e.target.value })
            }
          >
            <option value="">All Managers</option>
            {managers.map((manager) => (
              <option key={manager.id} value={manager.id}>
                {manager.name}
              </option>
            ))}
          </select>
        </div>
        <AddResultForm
          onResultAdded={fetchResults}
          leagues={leagues}
          managers={managers}
        />
      </div>
      <ul>
        {results.map((result) => (
          <li key={result.id}>
            {result.league_name} (
            {result.start_year === result.end_year
              ? result.start_year
              : `${result.start_year}-${result.end_year}`}
            ): G - {result.gold_manager_names.join(", ")}, S -{" "}
            {result.silver_manager_names.join(", ")}, B -{" "}
            {result.bronze_manager_names.join(", ")}
            <button onClick={() => handleEditResult(result)}>Edit</button>
            <button onClick={() => handleDeleteConfirmation(result)}>
              Delete
            </button>
          </li>
        ))}
      </ul>
      {selectedResult && (
        <EditResultDialog
          result={selectedResult}
          onClose={handleCloseDialog}
          onResultUpdated={fetchResults}
          leagues={leagues}
          managers={managers}
        />
      )}
      {deleteConfirmationOpen && (
        <DeleteConfirmationDialog
          onCancel={handleDeleteCancel}
          onConfirm={handleDeleteConfirm}
        />
      )}
    </div>
  );
}

export default ResultList;
