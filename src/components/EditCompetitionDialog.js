import React, { useState } from "react";
import axiosInstance from '../axiosConfig';
import "../styles/Dialog.css";

function EditCompetitionDialog({
  competition,
  onClose,
  onCompetitionUpdated,
  sports,
}) {
  const [name, setName] = useState(competition.name);
  const [sportId, setSportId] = useState(competition.sport_id);
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name.trim() === "" || sportId === "") {
      setError("Please fill in all fields");
      return;
    }
    try {
      await axiosInstance.put(
        `/competitions/${competition.id}`,
        {
          name,
          sportId,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      onCompetitionUpdated();
      onClose();
    } catch (error) {
      console.error("Error updating competition:", error);
    }
  };

  return (
    <div className="dialog-overlay" onClick={onClose}>
      <div className="dialog-content" onClick={(e) => e.stopPropagation()}>
        <h3>Edit Competition</h3>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Competition Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <select value={sportId} onChange={(e) => setSportId(e.target.value)}>
            <option value="">Select Sport</option>
            {sports.map((sport) => (
              <option key={sport.id} value={sport.id}>
                {sport.name}
              </option>
            ))}
          </select>
          {error && <p style={{ color: "red" }}>{error}</p>}
          <div className="dialog-actions">
            <button type="submit">Update</button>
            <button type="button" onClick={onClose}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditCompetitionDialog;
