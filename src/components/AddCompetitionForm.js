import React, { useState } from "react";
import axiosInstance from '../axiosConfig';

function AddCompetitionForm({ onCompetitionAdded, sports }) {
  const [name, setName] = useState("");
  const [sportId, setSportId] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name.trim() === "" || sportId === "") {
      setError("Please fill in all fields");
      return;
    }
    try {
      await axiosInstance.post(
        "/competitions",
        {
          name,
          sportId,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setName("");
      setSportId("");
      setError("");
      onCompetitionAdded();
    } catch (error) {
      console.error("Error adding competition:", error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Competition Name"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <select value={sportId} onChange={(e) => setSportId(e.target.value)}>
        <option value="">Select Sport</option>
        {sports.map((sport) => (
          <option key={sport.id} value={sport.id}>
            {sport.name}
          </option>
        ))}
      </select>
      {error && <p style={{ color: "red" }}>{error}</p>}
      <button type="submit">Add Competition</button>
    </form>
  );
}

export default AddCompetitionForm;
