import React, { useState } from "react";
import axiosInstance from '../axiosConfig';
import Select from "react-select";

function AddMedalTableConfigForm({ onConfigAdded, sports }) {
  const [name, setName] = useState("");
  const [sportId, setSportId] = useState(null);
  const [tier, setTier] = useState(null);
  const [displayOrder, setDisplayOrder] = useState(0);
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name.trim() === "") {
      setError("Please enter a name");
      return;
    }
    try {
      await axiosInstance.post(
        "/medal-table-configs",
        {
          name,
          sportId,
          tier,
          displayOrder,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setName("");
      setSportId(null);
      setTier(null);
      setDisplayOrder(0);
      setError("");
      onConfigAdded();
    } catch (error) {
      console.error("Error adding medal table config:", error);
    }
  };

  const sportOptions = sports.map((sport) => ({
    value: sport.id,
    label: sport.name,
  }));

  const tierOptions = [
    { value: 1, label: "Tier 1" },
    { value: 2, label: "Tier 2" },
    { value: 3, label: "Tier 3" },
  ];

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Name"
        value={name}
        onChange={(e) => setName(e.target.value)}
        required
      />
      <Select
        options={sportOptions}
        value={sportOptions.find((option) => option.value === sportId)}
        onChange={(selectedOption) =>
          setSportId(selectedOption ? selectedOption.value : null)
        }
        isClearable
        placeholder="Select Sport (optional)"
      />
      <Select
        options={tierOptions}
        value={tierOptions.find((option) => option.value === tier)}
        onChange={(selectedOption) =>
          setTier(selectedOption ? selectedOption.value : null)
        }
        isClearable
        placeholder="Select Tier (optional)"
      />
      <input
        type="number"
        placeholder="Display Order"
        value={displayOrder}
        onChange={(e) => setDisplayOrder(parseInt(e.target.value))}
        min={0}
      />
      {error && <p style={{ color: "red" }}>{error}</p>}
      <button type="submit">Add Configuration</button>
    </form>
  );
}

export default AddMedalTableConfigForm;
