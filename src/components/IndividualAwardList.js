import React, { useState, useEffect } from "react";
import axiosInstance from "../axiosConfig";
import AddIndividualAwardForm from "./AddIndividualAwardForm";
import EditIndividualAwardDialog from "./EditIndividualAwardDialog";
import DeleteConfirmationDialog from "./DeleteConfirmationDialog";

function IndividualAwardList() {
  const [individualAwards, setIndividualAwards] = useState([]);
  const [selectedIndividualAward, setSelectedIndividualAward] = useState(null);
  const [filter, setFilter] = useState({
    name: "",
  });
  const [timeoutId, setTimeoutId] = useState(null);
  const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const [individualAwardToDelete, setIndividualAwardToDelete] = useState(null);

  useEffect(() => {
    fetchIndividualAwards();
  }, []);

  useEffect(() => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    const newTimeoutId = setTimeout(() => {
      fetchIndividualAwards();
    }, 1000);
    setTimeoutId(newTimeoutId);
  }, [filter]);

  const fetchIndividualAwards = async () => {
    try {
      const response = await axiosInstance.get("/individual-awards", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        params: filter,
      });
      setIndividualAwards(response.data);
    } catch (error) {
      console.error("Error fetching individual awards:", error);
    }
  };

  const handleEditIndividualAward = (individualAward) => {
    setSelectedIndividualAward(individualAward);
  };

  const handleCloseDialog = () => {
    setSelectedIndividualAward(null);
  };

  const handleDeleteConfirmation = (individualAward) => {
    setIndividualAwardToDelete(individualAward);
    setDeleteConfirmationOpen(true);
  };

  const handleDeleteCancel = () => {
    setIndividualAwardToDelete(null);
    setDeleteConfirmationOpen(false);
  };

  const handleDeleteConfirm = async () => {
    try {
      await axiosInstance.delete(
        `/individual-awards/${individualAwardToDelete.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      fetchIndividualAwards();
      setDeleteConfirmationOpen(false);
    } catch (error) {
      console.error("Error deleting individual award:", error);
    }
  };

  return (
    <div>
      <h2>Individual Award List</h2>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div>
          <input
            type="text"
            placeholder="Filter by name"
            value={filter.name}
            onChange={(e) => setFilter({ ...filter, name: e.target.value })}
          />
        </div>
        <AddIndividualAwardForm
          onIndividualAwardAdded={fetchIndividualAwards}
        />
      </div>
      <ul>
        {individualAwards.map((individualAward) => (
          <li key={individualAward.id}>
            {individualAward.name}
            <button onClick={() => handleEditIndividualAward(individualAward)}>
              Edit
            </button>
            <button onClick={() => handleDeleteConfirmation(individualAward)}>
              Delete
            </button>
          </li>
        ))}
      </ul>
      {selectedIndividualAward && (
        <EditIndividualAwardDialog
          individualAward={selectedIndividualAward}
          onClose={handleCloseDialog}
          onIndividualAwardUpdated={fetchIndividualAwards}
        />
      )}
      {deleteConfirmationOpen && (
        <DeleteConfirmationDialog
          onCancel={handleDeleteCancel}
          onConfirm={handleDeleteConfirm}
        />
      )}
    </div>
  );
}

export default IndividualAwardList;
