import React, { useState, useEffect } from "react";
import axiosInstance from "../axiosConfig";
import AddIndividualAwardResultForm from "./AddIndividualAwardResultForm";
import EditIndividualAwardResultDialog from "./EditIndividualAwardResultDialog";
import DeleteConfirmationDialog from "./DeleteConfirmationDialog";

function IndividualAwardResultList() {
  const [individualAwardResults, setIndividualAwardResults] = useState([]);
  const [selectedIndividualAwardResult, setSelectedIndividualAwardResult] =
    useState(null);
  const [filter, setFilter] = useState({
    individualAwardId: "",
    managerId: "",
    startYear: "",
    endYear: "",
  });
  const [individualAwards, setIndividualAwards] = useState([]);
  const [managers, setManagers] = useState([]);
  const [timeoutId, setTimeoutId] = useState(null);
  const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const [individualAwardResultToDelete, setIndividualAwardResultToDelete] =
    useState(null);

  useEffect(() => {
    fetchIndividualAwardResults();
    fetchIndividualAwards();
    fetchManagers();
  }, []);

  useEffect(() => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    const newTimeoutId = setTimeout(() => {
      fetchIndividualAwardResults();
    }, 1000);
    setTimeoutId(newTimeoutId);
  }, [filter]);

  const fetchIndividualAwardResults = async () => {
    try {
      const response = await axiosInstance.get("/individual-award-results", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        params: filter,
      });
      setIndividualAwardResults(response.data);
    } catch (error) {
      console.error("Error fetching individual award results:", error);
    }
  };

  const fetchIndividualAwards = async () => {
    try {
      const response = await axiosInstance.get("/individual-awards", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setIndividualAwards(response.data);
    } catch (error) {
      console.error("Error fetching individual awards:", error);
    }
  };

  const fetchManagers = async () => {
    try {
      const response = await axiosInstance.get("/managers", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setManagers(response.data);
    } catch (error) {
      console.error("Error fetching managers:", error);
    }
  };

  const handleEditIndividualAwardResult = (individualAwardResult) => {
    setSelectedIndividualAwardResult(individualAwardResult);
  };

  const handleCloseDialog = () => {
    setSelectedIndividualAwardResult(null);
  };

  const handleDeleteConfirmation = (individualAwardResult) => {
    setIndividualAwardResultToDelete(individualAwardResult);
    setDeleteConfirmationOpen(true);
  };

  const handleDeleteCancel = () => {
    setIndividualAwardResultToDelete(null);
    setDeleteConfirmationOpen(false);
  };

  const handleDeleteConfirm = async () => {
    try {
      await axiosInstance.delete(
        `/individual-award-results/${individualAwardResultToDelete.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      fetchIndividualAwardResults();
      setDeleteConfirmationOpen(false);
    } catch (error) {
      console.error("Error deleting individual award result:", error);
    }
  };

  return (
    <div>
      <h2>Individual Award Result List</h2>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div>
          <select
            value={filter.individualAwardId}
            onChange={(e) =>
              setFilter({ ...filter, individualAwardId: e.target.value })
            }
          >
            <option value="">All Individual Awards</option>
            {individualAwards.map((individualAward) => (
              <option key={individualAward.id} value={individualAward.id}>
                {individualAward.name}
              </option>
            ))}
          </select>
          <select
            value={filter.managerId}
            onChange={(e) =>
              setFilter({ ...filter, managerId: e.target.value })
            }
          >
            <option value="">All Managers</option>
            {managers.map((manager) => (
              <option key={manager.id} value={manager.id}>
                {manager.name}
              </option>
            ))}
          </select>
          <input
            type="text"
            placeholder="Start Year (YYYY)"
            value={filter.startYear}
            onChange={(e) =>
              setFilter({ ...filter, startYear: e.target.value })
            }
          />
          <input
            type="text"
            placeholder="End Year (YYYY)"
            value={filter.endYear}
            onChange={(e) => setFilter({ ...filter, endYear: e.target.value })}
          />
        </div>
        <AddIndividualAwardResultForm
          onIndividualAwardResultAdded={fetchIndividualAwardResults}
          individualAwards={individualAwards}
          managers={managers}
        />
      </div>
      <ul>
        {individualAwardResults.map((individualAwardResult) => (
          <li key={individualAwardResult.id}>
            {individualAwardResult.individual_award_name} (
            {individualAwardResult.start_year === individualAwardResult.end_year
              ? individualAwardResult.start_year
              : `${individualAwardResult.start_year}-${individualAwardResult.end_year}`}
            ) - {individualAwardResult.manager_name}
            <button
              onClick={() =>
                handleEditIndividualAwardResult(individualAwardResult)
              }
            >
              Edit
            </button>
            <button
              onClick={() => handleDeleteConfirmation(individualAwardResult)}
            >
              Delete
            </button>
          </li>
        ))}
      </ul>
      {selectedIndividualAwardResult && (
        <EditIndividualAwardResultDialog
          individualAwardResult={selectedIndividualAwardResult}
          onClose={handleCloseDialog}
          onIndividualAwardResultUpdated={fetchIndividualAwardResults}
          individualAwards={individualAwards}
          managers={managers}
        />
      )}
      {deleteConfirmationOpen && (
        <DeleteConfirmationDialog
          onCancel={handleDeleteCancel}
          onConfirm={handleDeleteConfirm}
        />
      )}
    </div>
  );
}

export default IndividualAwardResultList;
