import React, { useState, useEffect } from "react";
import axiosInstance from "../axiosConfig";
import "../styles/MedalTableScreen.css";

function MedalTableScreen() {
  const [medalTables, setMedalTables] = useState([]);
  const [resultLogs, setResultLogs] = useState([]);
  const [individualAwardResultLogs, setIndividualAwardResultLogs] = useState(
    []
  );

  useEffect(() => {
    fetchMedalTables();
    fetchResultLogs();
    fetchIndividualAwardResultLogs();
  }, []);

  const fetchMedalTables = async () => {
    try {
      const response = await axiosInstance.get("/medal-tables");
      setMedalTables(response.data);
    } catch (error) {
      console.error("Error fetching medal tables:", error);
    }
  };

  const fetchResultLogs = async () => {
    try {
      const response = await axiosInstance.get("/results-logs");
      setResultLogs(response.data);
    } catch (error) {
      console.error("Error fetching result logs:", error);
    }
  };

  const fetchIndividualAwardResultLogs = async () => {
    try {
      const response = await axiosInstance.get(
        "/individual-award-results-logs"
      );
      setIndividualAwardResultLogs(response.data);
    } catch (error) {
      console.error("Error fetching individual award result logs:", error);
    }
  };

  const handleExport = async () => {
    try {
      const response = await axiosInstance.get("/medal-tables/export", {
        responseType: "blob",
      });
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "medal-tables.txt");
      document.body.appendChild(link);
      link.click();
    } catch (error) {
      console.error("Error exporting medal tables:", error);
    }
  };

  return (
    <div className="medal-table-screen">
      <h2>Medal Tables</h2>
      {medalTables.map((table) => (
        <div key={table.configId} className="medal-table">
          <h3>{table.configName}</h3>
          <table>
            <thead>
              <tr>
                <th>Rank</th>
                <th>Manager</th>
                <th>Gold</th>
                <th>Silver</th>
                <th>Bronze</th>
              </tr>
            </thead>
            <tbody>
              {table.rows.map((row, index) => (
                <tr
                  key={row.managerId}
                  className={index % 2 === 0 ? "even" : "odd"}
                >
                  <td>{row.rank}</td>
                  <td>{row.managerName}</td>
                  <td>{row.gold}</td>
                  <td>{row.silver}</td>
                  <td>{row.bronze}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      ))}
      <h2>Computed Result Logs</h2>
      <ul>
        {resultLogs.map((result) => (
          <li key={result.id}>
            {result.league_name} (
            {result.start_year === result.end_year
              ? result.start_year
              : `${result.start_year}-${result.end_year}`}
            ): G - {result.gold_manager_names.join(", ")}, S -{" "}
            {result.silver_manager_names.join(", ")}, B -{" "}
            {result.bronze_manager_names.join(", ")}
          </li>
        ))}
      </ul>
      <h2>Individual Awards</h2>
      <ul>
        {individualAwardResultLogs.map((result) => (
          <li key={result.id}>
            {result.individual_award_name} (
            {result.start_year === result.end_year
              ? result.start_year
              : `${result.start_year}-${result.end_year}`}
            ): {result.manager_name} ({result.award_count})
          </li>
        ))}
      </ul>
      <button className="export-button" onClick={handleExport}>
        Export
      </button>
    </div>
  );
}

export default MedalTableScreen;
