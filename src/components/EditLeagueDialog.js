import React, { useState } from "react";
import axiosInstance from '../axiosConfig';
import "../styles/Dialog.css";

function EditLeagueDialog({ league, onClose, onLeagueUpdated, competitions }) {
  const [name, setName] = useState(league.name);
  const [competitionId, setCompetitionId] = useState(league.competition_id);
  const [tier, setTier] = useState(league.tier);
  const [startYear, setStartYear] = useState(league.start_year.toString());
  const [endYear, setEndYear] = useState(league.end_year.toString());
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      name.trim() === "" ||
      competitionId === "" ||
      tier === "" ||
      startYear === "" ||
      endYear === ""
    ) {
      setError("Please fill in all fields");
      return;
    }
    if (isNaN(startYear) || startYear.length !== 4) {
      setError("Start Year must be a valid year (YYYY)");
      return;
    }
    if (isNaN(endYear) || endYear.length !== 4) {
      setError("End Year must be a valid year (YYYY)");
      return;
    }
    if (parseInt(endYear) < parseInt(startYear)) {
      setError("End Year must be greater than or equal to Start Year");
      return;
    }
    try {
      await axiosInstance.put(
        `/leagues/${league.id}`,
        {
          name,
          competitionId,
          tier,
          startYear: parseInt(startYear),
          endYear: parseInt(endYear),
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      onLeagueUpdated();
      onClose();
    } catch (error) {
      console.error("Error updating league:", error);
    }
  };

  return (
    <div className="dialog-overlay" onClick={onClose}>
      <div className="dialog-content" onClick={(e) => e.stopPropagation()}>
        <h3>Edit League</h3>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="League Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <select
            value={competitionId}
            onChange={(e) => setCompetitionId(e.target.value)}
          >
            <option value="">Select Competition</option>
            {competitions.map((competition) => (
              <option key={competition.id} value={competition.id}>
                {competition.name} ({competition.sport_name})
              </option>
            ))}
          </select>
          <select value={tier} onChange={(e) => setTier(e.target.value)}>
            <option value="">Select Tier</option>
            <option value="1">Tier 1</option>
            <option value="2">Tier 2</option>
            <option value="3">Tier 3</option>
          </select>
          <input
            type="text"
            placeholder="Start Year (YYYY)"
            value={startYear}
            onChange={(e) => setStartYear(e.target.value)}
          />
          <input
            type="text"
            placeholder="End Year (YYYY)"
            value={endYear}
            onChange={(e) => setEndYear(e.target.value)}
          />
          {error && <p style={{ color: "red" }}>{error}</p>}
          <div className="dialog-actions">
            <button type="submit">Update</button>
            <button type="button" onClick={onClose}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditLeagueDialog;
