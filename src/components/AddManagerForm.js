import React, { useState } from "react";
import axiosInstance from '../axiosConfig';

function AddManagerForm({ onManagerAdded }) {
  const [name, setName] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name.trim() === "") {
      setError("Manager name cannot be blank");
      return;
    }
    try {
      await axiosInstance.post(
        "/managers",
        { name },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setName("");
      setError("");
      onManagerAdded();
    } catch (error) {
      console.error("Error adding manager:", error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Manager Name"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      {error && <p style={{ color: "red" }}>{error}</p>}
      <button type="submit">Register</button>
    </form>
  );
}

export default AddManagerForm;
