import React, { useState } from "react";
import axiosInstance from "../axiosConfig";
import "../styles/Dialog.css";

function EditIndividualAwardDialog({
  individualAward,
  onClose,
  onIndividualAwardUpdated,
}) {
  const [name, setName] = useState(individualAward.name);
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name.trim() === "") {
      setError("Please fill in all fields");
      return;
    }    
    try {
      await axiosInstance.put(
        `/individual-awards/${individualAward.id}`,
        {
          name,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      onIndividualAwardUpdated();
      onClose();
    } catch (error) {
      console.error("Error updating individual award:", error);
    }
  };

  return (
    <div className="dialog-overlay" onClick={onClose}>
      <div className="dialog-content" onClick={(e) => e.stopPropagation()}>
        <h3>Edit Individual Award</h3>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Individual Award Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          {error && <p style={{ color: "red" }}>{error}</p>}
          <div className="dialog-actions">
            <button type="submit">Update</button>
            <button type="button" onClick={onClose}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditIndividualAwardDialog;
