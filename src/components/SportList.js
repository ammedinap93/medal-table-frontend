import React, { useState, useEffect } from "react";
import axiosInstance from '../axiosConfig';
import AddSportForm from "./AddSportForm";
import EditSportDialog from "./EditSportDialog";
import DeleteConfirmationDialog from "./DeleteConfirmationDialog";

function SportList() {
  const [sports, setSports] = useState([]);
  const [selectedSport, setSelectedSport] = useState(null);
  const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const [sportToDelete, setSportToDelete] = useState(null);

  useEffect(() => {
    fetchSports();
  }, []);

  const fetchSports = async () => {
    try {
      const response = await axiosInstance.get("/sports", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setSports(response.data);
    } catch (error) {
      console.error("Error fetching sports:", error);
    }
  };

  const handleEditSport = (sport) => {
    setSelectedSport(sport);
  };

  const handleCloseDialog = () => {
    setSelectedSport(null);
  };

  const handleDeleteConfirmation = (sport) => {
    setSportToDelete(sport);
    setDeleteConfirmationOpen(true);
  };

  const handleDeleteCancel = () => {
    setSportToDelete(null);
    setDeleteConfirmationOpen(false);
  };

  const handleDeleteConfirm = async () => {
    try {
      await axiosInstance.delete(`/sports/${sportToDelete.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      fetchSports();
      setDeleteConfirmationOpen(false);
    } catch (error) {
      console.error("Error deleting sport:", error);
    }
  };

  return (
    <div>
      <h2>Sport List</h2>
      <AddSportForm onSportAdded={fetchSports} />
      <ul>
        {sports.map((sport) => (
          <li key={sport.id}>
            {sport.name}
            <button onClick={() => handleEditSport(sport)}>Edit</button>
            <button onClick={() => handleDeleteConfirmation(sport)}>
              Delete
            </button>
          </li>
        ))}
      </ul>
      {selectedSport && (
        <EditSportDialog
          sport={selectedSport}
          onClose={handleCloseDialog}
          onSportUpdated={fetchSports}
        />
      )}
      {deleteConfirmationOpen && (
        <DeleteConfirmationDialog
          onCancel={handleDeleteCancel}
          onConfirm={handleDeleteConfirm}
        />
      )}
    </div>
  );
}

export default SportList;
