import React, { useState } from "react";
import axiosInstance from '../axiosConfig';
import "../styles/Dialog.css";

function EditSportDialog({ sport, onClose, onSportUpdated }) {
  const [name, setName] = useState(sport.name);
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name.trim() === "") {
      setError("Sport name cannot be blank");
      return;
    }
    try {
      await axiosInstance.put(
        `/sports/${sport.id}`,
        { name },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      onSportUpdated();
      onClose();
    } catch (error) {
      console.error("Error updating sport:", error);
    }
  };

  return (
    <div className="dialog-overlay" onClick={onClose}>
      <div className="dialog-content" onClick={(e) => e.stopPropagation()}>
        <h3>Edit Sport</h3>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Sport Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          {error && <p style={{ color: "red" }}>{error}</p>}
          <div className="dialog-actions">
            <button type="submit">Update</button>
            <button type="button" onClick={onClose}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditSportDialog;
