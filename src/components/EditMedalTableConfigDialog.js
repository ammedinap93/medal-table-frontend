import React, { useState } from "react";
import axiosInstance from '../axiosConfig';
import Select from "react-select";
import "../styles/Dialog.css";

function EditMedalTableConfigDialog({
  config,
  onClose,
  onConfigUpdated,
  sports,
}) {
  const [name, setName] = useState(config.name);
  const [sportId, setSportId] = useState(config.sport_id);
  const [tier, setTier] = useState(config.tier);
  const [displayOrder, setDisplayOrder] = useState(config.display_order);
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name.trim() === "") {
      setError("Please enter a name");
      return;
    }
    try {
      await axiosInstance.put(
        `/medal-table-configs/${config.id}`,
        {
          name,
          sportId,
          tier,
          displayOrder,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      onConfigUpdated();
      onClose();
    } catch (error) {
      console.error("Error updating medal table config:", error);
    }
  };

  const sportOptions = sports.map((sport) => ({
    value: sport.id,
    label: sport.name,
  }));

  const tierOptions = [
    { value: 1, label: "Tier 1" },
    { value: 2, label: "Tier 2" },
    { value: 3, label: "Tier 3" },
  ];

  return (
    <div className="dialog-overlay" onClick={onClose}>
      <div className="dialog-content" onClick={(e) => e.stopPropagation()}>
        <h3>Edit Medal Table Configuration</h3>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
          <Select
            options={sportOptions}
            value={sportOptions.find((option) => option.value === sportId)}
            onChange={(selectedOption) =>
              setSportId(selectedOption ? selectedOption.value : null)
            }
            isClearable
            placeholder="Select Sport (optional)"
          />
          <Select
            options={tierOptions}
            value={tierOptions.find((option) => option.value === tier)}
            onChange={(selectedOption) =>
              setTier(selectedOption ? selectedOption.value : null)
            }
            isClearable
            placeholder="Select Tier (optional)"
          />
          <input
            type="number"
            placeholder="Display Order"
            value={displayOrder}
            onChange={(e) => setDisplayOrder(parseInt(e.target.value))}
            min={0}
          />
          {error && <p style={{ color: "red" }}>{error}</p>}
          <div className="dialog-actions">
            <button type="submit">Update</button>
            <button type="button" onClick={onClose}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditMedalTableConfigDialog;
