import React, { useState } from "react";
import axiosInstance from "../axiosConfig";
import Select from "react-select";
import "../styles/Dialog.css";

function EditIndividualAwardResultDialog({
  individualAwardResult,
  onClose,
  onIndividualAwardResultUpdated,
  individualAwards,
  managers,
}) {
  const [individualAwardId, setIndividualAwardId] = useState(
    individualAwardResult.individual_award_id
  );
  const [managerId, setManagerId] = useState(individualAwardResult.manager_id);
  const [startYear, setStartYear] = useState(
    individualAwardResult.start_year.toString()
  );
  const [endYear, setEndYear] = useState(
    individualAwardResult.end_year.toString()
  );
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      individualAwardId === "" ||
      managerId === "" ||
      startYear === "" ||
      endYear === ""
    ) {
      setError("Please fill in all fields");
      return;
    }
    if (isNaN(startYear) || startYear.length !== 4) {
      setError("Start Year must be a valid year (YYYY)");
      return;
    }
    if (isNaN(endYear) || endYear.length !== 4) {
      setError("End Year must be a valid year (YYYY)");
      return;
    }
    if (parseInt(endYear) < parseInt(startYear)) {
      setError("End Year must be greater than or equal to Start Year");
      return;
    }
    try {
      await axiosInstance.put(
        `/individual-award-results/${individualAwardResult.id}`,
        {
          individualAwardId,
          managerId,
          startYear: parseInt(startYear),
          endYear: parseInt(endYear),
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      onIndividualAwardResultUpdated();
      onClose();
    } catch (error) {
      console.error("Error updating individual award result:", error);
    }
  };

  const individualAwardOptions = individualAwards.map((individualAward) => ({
    value: individualAward.id,
    label: individualAward.name,
  }));

  const managerOptions = managers.map((manager) => ({
    value: manager.id,
    label: manager.name,
  }));

  return (
    <div className="dialog-overlay" onClick={onClose}>
      <div className="dialog-content" onClick={(e) => e.stopPropagation()}>
        <h3>Edit Individual Award Result</h3>
        <form onSubmit={handleSubmit}>
          <Select
            options={individualAwardOptions}
            value={individualAwardOptions.find(
              (option) => option.value === individualAwardId
            )}
            onChange={(selectedOption) =>
              setIndividualAwardId(selectedOption.value)
            }
            placeholder="Select Individual Award"
            required
          />
          <Select
            options={managerOptions}
            value={managerOptions.find((option) => option.value === managerId)}
            onChange={(selectedOption) => setManagerId(selectedOption.value)}
            placeholder="Select Manager"
            required
          />
          <input
            type="text"
            placeholder="Start Year (YYYY)"
            value={startYear}
            onChange={(e) => setStartYear(e.target.value)}
          />
          <input
            type="text"
            placeholder="End Year (YYYY)"
            value={endYear}
            onChange={(e) => setEndYear(e.target.value)}
          />
          {error && <p style={{ color: "red" }}>{error}</p>}
          <div className="dialog-actions">
            <button type="submit">Update</button>
            <button type="button" onClick={onClose}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditIndividualAwardResultDialog;
