import * as React from "react";
import { createRoot } from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
} from "react-router-dom";
import ManagerList from "./components/ManagerList";
import SportList from "./components/SportList";
import CompetitionList from "./components/CompetitionList";
import LeagueList from "./components/LeagueList";
import ResultList from "./components/ResultList";
import MedalTableConfigList from "./components/MedalTableConfigList";
import MedalTableScreen from "./components/MedalTableScreen";
import LoginForm from "./components/LoginForm";
import IndividualAwardList from "./components/IndividualAwardList";
import IndividualAwardResultList from "./components/IndividualAwardResultList";

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <div>
        <h1>Welcome to Medal Table!</h1>
        <Link to="managers">Managers</Link>
        <br />
        <Link to="sports">Sports</Link>
        <br />
        <Link to="competitions">Competitions</Link>
        <br />
        <Link to="leagues">Leagues</Link>
        <br />
        <Link to="results">Results</Link>
        <br />
        <Link to="medal-table-configs">Medal Table Configs</Link>
        <br />
        <Link to="medal-tables">Medal Tables</Link>
        <br />
        <Link to="individual-awards">Individual Awards</Link>
        <br />
        <Link to="individual-award-results">Individual Award Results</Link>
      </div>
    ),
  },
  {
    path: "/login",
    element: <LoginForm />,
  },
  {
    path: "managers",
    element: <ManagerList />,
  },
  {
    path: "sports",
    element: <SportList />,
  },
  {
    path: "competitions",
    element: <CompetitionList />,
  },
  {
    path: "leagues",
    element: <LeagueList />,
  },
  {
    path: "results",
    element: <ResultList />,
  },
  {
    path: "medal-table-configs",
    element: <MedalTableConfigList />,
  },
  {
    path: "medal-tables",
    element: <MedalTableScreen />,
  },
  {
    path: "individual-awards",
    element: <IndividualAwardList />,
  },
  {
    path: "individual-award-results",
    element: <IndividualAwardResultList />,
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
