import axios from "axios";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_SERVER_URL,
});

axiosInstance.interceptors.response.use(
  (response) => response,
  (error) => {
    if (
      error.response &&
      (error.response.status === 401 || error.response.status === 403)
    ) {
      // Clear the token from localStorage
      localStorage.removeItem("token");

      // Redirect to the login screen
      window.location.href = "/login";
    }
    return Promise.reject(error);
  }
);

export default axiosInstance;
