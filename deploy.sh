#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER
SERVER_FOLDER="medal_table_fe"

# Building React output
yarn install
CI=false yarn run build

echo "Deploying to ${DEPLOY_SERVER}"
scp -r build/ ubuntu@${DEPLOY_SERVER}:/var/www/html/${SERVER_FOLDER}/

echo "Finished copying the build files"
